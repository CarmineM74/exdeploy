# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :exdeploy,
  ecto_repos: [Exdeploy.Repo]

# Configures the endpoint
config :exdeploy, ExdeployWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "K91wGxPD8W3wFN80Z3rF94gE/PO6w8/99Kyhat43c17DH2tpZYmDQxjKdsMn8T/o",
  render_errors: [view: ExdeployWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Exdeploy.PubSub,
  live_view: [signing_salt: "RFuanbxn"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
