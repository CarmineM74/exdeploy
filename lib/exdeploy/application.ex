defmodule Exdeploy.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Exdeploy.Repo,
      # Start the Telemetry supervisor
      ExdeployWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Exdeploy.PubSub},
      # Start the Endpoint (http/https)
      ExdeployWeb.Endpoint
      # Start a worker by calling: Exdeploy.Worker.start_link(arg)
      # {Exdeploy.Worker, arg}
    ]

    opts = [strategy: :one_for_one, name: Exdeploy.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    ExdeployWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
