defmodule Exdeploy.Repo do
  use Ecto.Repo,
    otp_app: :exdeploy,
    adapter: Ecto.Adapters.Postgres
end
