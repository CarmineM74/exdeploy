defmodule ExdeployWeb.PageController do
  use ExdeployWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
